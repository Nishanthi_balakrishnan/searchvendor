package utils;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataInputProvider {
	
	public Object[][] getData(String dataSheetName)
	{
		Object[][] data = null ;
		
		try
		{
			
			XSSFWorkbook wbook= new XSSFWorkbook(dataSheetName);
			XSSFSheet sheet = wbook.getSheetAt(0);
			//get row count
			int rowcount = sheet.getLastRowNum();
			
			//get column count
			int colcount=sheet.getRow(0).getLastCellNum();
			
			for(int i=1;i<=rowcount;i++)
			{
				XSSFRow row = sheet.getRow(i);
				for(int j=1;j<colcount;j++)
				{
					try {
						String cellValue = "";
						try{
							cellValue = row.getCell(j).getStringCellValue();
						}catch(NullPointerException e){

						}
						data[i-1][j]  = cellValue; // add to the data array
					} catch (Exception e) {
						e.printStackTrace();
					}				
				}
				}
				
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		return null;
		
	}

}
