package base.design;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;


public class SeleniumBase implements Browser,Element {

	WebDriver driver;
	int number=0;
	
	@Override
	public void click(WebElement ele) {
		// TODO Auto-generated method stub
		
		click(ele);
		
	}

	@Override
	public void append(WebElement ele, String data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear(WebElement ele) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clearAndType(WebElement ele, String data) {
		
	ele.clear();
	ele.sendKeys(data);
	System.out.println("Entered value : "+data);
		
	}

	@Override
	public String getElementText(WebElement ele) {
		// TODO Auto-generated method stub
		
		String txt = ele.getText();
		
		return txt;
	}

	@Override
	public String getBackgroundColor(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getTypedText(WebElement ele) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void selectDropDownUsingValue(WebElement ele, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyExactText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyDisappeared(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyEnabled(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifySelected(WebElement ele) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void startApp(String url) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void startApp(String browser, String url) {
		// TODO Auto-generated method stub
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		//Launch chrome
		driver= new ChromeDriver();
		
		
		//Launch url
		driver.get("https://acme-test.uipath.com/account/login");
		
		//maximize the window
		
		driver.manage().window().maximize();
		
		//set timeout
		driver.manage().timeouts().implicitlyWait(2000,TimeUnit.MILLISECONDS);
		
	}

	@Override
	public WebElement locateElement(String locatorType, String value) {
		
		try {
			switch (locatorType) {
			case "id": return driver.findElement(By.id(value));
			case "name": return driver.findElement(By.name(value));
			case "class": return driver.findElement(By.className(value));
			case "link": return driver.findElement(By.linkText(value));
			case "tagName": return driver.findElement(By.tagName(value));
			case "xpath": return driver.findElement(By.xpath(value));
			case "css": return driver.findElement(By.cssSelector(value));
			}
		}  catch (NoSuchElementException e) {
			//logStep("The element with locator "+locatorType+" not found.","fail");
			System.out.println("The element with locator "+locatorType+" not found.");
		} catch (WebDriverException e) {
			//logStep("Unknown exception occured while finding "+locatorType+" with the value "+value, "fail");
			System.out.println("Unknown exception occured while finding "+locatorType+" with the value "+value);
		}
		return null;
		
	}

	@Override
	public WebElement locateElement(String value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<WebElement> locateElements(String type, String value) {
		try {
			switch (type) {
			case "id": return driver.findElements(By.id(value));
			case "name": return driver.findElements(By.name(value));
			case "class": return driver.findElements(By.className(value));
			case "link": return driver.findElements(By.linkText(value));
			case "tagName": return driver.findElements(By.tagName(value));
			case "xpath": return driver.findElements(By.xpath(value));
			case "css": return driver.findElements(By.cssSelector(value));
			}
		}  catch (NoSuchElementException e) {
			//logStep("The element with locator "+locatorType+" not found.","fail");
			System.out.println("The element with locator "+type+" not found.");
		} catch (WebDriverException e) {
			//logStep("Unknown exception occured while finding "+locatorType+" with the value "+value, "fail");
			System.out.println("Unknown exception occured while finding "+type+" with the value "+value);
		}
		return null;
	}

	@Override
	public void switchToAlert() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void acceptAlert() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void typeAlert(String data) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void switchToWindow(int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void switchToWindow(String title) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void switchToFrame(int index) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void switchToFrame(String idOrName) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void defaultContent() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean verifyUrl(String url) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean verifyTitle(String title) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long takeSnap() {
		long number = (long) Math.floor(Math.random() * 900000000L) + 10000000L; 
		try {
			FileUtils.copyFile(((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE) , new File("./reports/images/"+number+".jpg"));
		} catch (WebDriverException e) {
			System.out.println("The browser has been closed.");
		} catch (IOException e) {
			System.out.println("The snapshot could not be taken");
		}
		return number;
	}

	@Override
	public void close() {
		driver.close();
		
	}

	@Override
	public void quit() {
		// TODO Auto-generated method stub
		
	}
	
	

}
