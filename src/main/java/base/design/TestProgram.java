package base.design;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;

public class TestProgram {
	
	WebDriver driver;
	@Test
	public void searchVendor() throws InterruptedException
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		//Launch chrome
		driver= new ChromeDriver();
		
		
		//Launch url
		driver.get("https://acme-test.uipath.com/account/login");
		
		//maximize the window
		
		driver.manage().window().maximize();
		
		//set timeout
		driver.manage().timeouts().implicitlyWait(2000,TimeUnit.MILLISECONDS);
		
		//enter email
		driver.findElement(By.id("email")).sendKeys("nishanthimayaa@gmail.com");
		
		
		//enter password
		
		driver.findElement(By.id("password")).sendKeys("shanthi1234");
		
		//click login
		
		driver.findElement(By.id("buttonLogin")).click();
		
		//Click on vendors
		WebElement eleVendor = driver.findElement(By.xpath("//button[text()=' Vendors']"));
		
		Actions act=new Actions(driver);
		act.moveToElement(eleVendor).perform();
		
		Thread.sleep(1000);
		
		
		//Click on search vendor
		//xpath - //a[text()='Search for Vendor']
		driver.findElement(By.linkText("Search for Vendor")).click();
		
		//RO874231
		//enter vendor id
		driver.findElement(By.id("vendorTaxID")).sendKeys("RO874231");
		
		//click on search
		driver.findElement(By.id("buttonSearch")).click();
		
		//Navigate to table
		WebElement eleTable = driver.findElement(By.xpath("//table[@class='table']"));
		
		 List<WebElement> rows = eleTable.findElements(By.tagName("tr"));
		 
		 WebElement eleFirstRow = rows.get(1);
		 
		 List<WebElement> cells = eleFirstRow.findElements(By.tagName("td"));
		 //get Venodr
		 String txtVendor = cells.get(0).getText();
		 System.out.println("Vendor name : "+ txtVendor);
		 
		
	}

}
